﻿using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Crossovers;
using GeneticSharp.Domain.Randomizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace msiag.Models
{
    class CowgcCrossover : CrossoverBase
    {
        public CowgcCrossover() : base(2, 2)
        {
            IsOrdered = true;
        }

        protected override IList<IChromosome> PerformCross(IList<IChromosome> parents)
        {
            var parentOne = parents[0];
            var parentTwo = parents[1];

            int cutPointP1 = 0;
            int cutPointP2 = 0;
            double distanceP1 = 0.0;
            double distanceP2 = 0.0;

            IChromosome firstChild;
            IChromosome secondChild;

            if (parents.AnyHasRepeatedGene())
            {
                throw new CrossoverException(this, "The Ordered Crossover (OX1) can be only used with ordered chromosomes. The specified chromosome has repeated genes.");
            }

            for (int i = 1; i < parentOne.Length; i++)
            {
                City currentCity = parentOne.GetGene(i).Value as City;
                City previousCity = parentOne.GetGene(i - 1).Value as City;

                double distance = previousCity.GetDistance(currentCity);

                if (distance > distanceP1)
                {
                    distanceP1 = distance;
                    cutPointP1 = i;
                }
            }

            for (int i = 1; i < parentTwo.Length; i++)
            {
                City currentCity = parentTwo.GetGene(i).Value as City;
                City previousCity = parentTwo.GetGene(i - 1).Value as City;

                double distance = previousCity.GetDistance(currentCity);

                if (distance > distanceP2)
                {
                    distanceP2 = distance;
                    cutPointP2 = i;
                }
            }

            if (distanceP1 > distanceP2)
            {
                firstChild = CreateChild(parentOne, parentTwo, cutPointP1);
                secondChild = CreateChild(parentTwo, parentOne, cutPointP1);
            } else
            {
                firstChild = CreateChild(parentOne, parentTwo, cutPointP2);
                secondChild = CreateChild(parentTwo, parentOne, cutPointP2);
            }
                
            return new List<IChromosome>() { firstChild, secondChild };
        }

        private static IChromosome CreateChild(IChromosome firstParent, IChromosome secondParent, int cutPoint)
        {
            var genesFromFirstParent = firstParent.GetGenes().Take(cutPoint).ToArray();
            var genesFromSeeondParent = secondParent.GetGenes().Except(genesFromFirstParent).ToArray();
            List<Gene> newGenes = new List<Gene>();
            newGenes.AddRange(genesFromFirstParent);
            newGenes.AddRange(genesFromSeeondParent);

            var child = firstParent.CreateNew();
            child.ReplaceGenes(0, newGenes.ToArray());
           
            return child;
        }
    }
}
