﻿using GeneticSharp.Domain.Chromosomes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace msiag.Models
{
    public class MyChromosome : ChromosomeBase
    {
        public double Distance { get; internal set; }
        public MyChromosome(int numberOfCities) : base(numberOfCities)
        {

        }
        public override IChromosome CreateNew()
        {
            var genes = this.GetGenes();
            var shuffled = genes.OrderBy(x => Guid.NewGuid()).ToArray();

            IChromosome newChromosome = new MyChromosome(shuffled.Count());

            newChromosome.ReplaceGenes(0, shuffled);

            return newChromosome;

        }

        public override Gene GenerateGene(int geneIndex)
        {
            throw new NotImplementedException();
        }
    }
}