﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace msiag.Models
{
    class City
    {
        //static int GlobalId = 0;
        public int Id { get; set; }

        public string Name { get; set; }
        public List<int> Distances;

        public City(int id, string name)
        {
            Id = id;
            Name = name;
            Distances = new List<int>();
        }

        public int GetDistance(City city)
        {
            int distance = this.Distances[city.Id];
            return distance;
        }

        public override string ToString()
        {
            return Name;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var item = obj as City;
            return Equals(item);
        }

        public bool Equals(City other)
        {
            return this.Id == other.Id;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Id.GetHashCode();
                return hashCode;
            }
        }
    }
}
