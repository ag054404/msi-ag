﻿using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Fitnesses;
using GeneticSharp.Extensions.Tsp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace msiag.Models
{
    internal class MyFitness : IFitness
    {
        private int ExpectedDistance { get; set; }
        private List<City> Cities { get; set; }
        public MyFitness(int expectedDistance, List<City> cities)
        {
            ExpectedDistance = expectedDistance;
            Cities = cities;
        }
        public double Evaluate(IChromosome chromosome)
        {
            var genes = chromosome.GetGenes();
            double totalDistance = 0.0;
            double fitness = 0.0;

            City firstCity = (City) genes.First().Value;
            City previousCity = firstCity;

            foreach(Gene gene in  genes )
            {
                City currentCity = (City)gene.Value;

                totalDistance += previousCity.GetDistance(currentCity);

                previousCity = currentCity;

            }

            totalDistance += previousCity.GetDistance(firstCity);

            //fitness = 1.0 - (totalDistance / (genes.Length * 1000.0));
            fitness = ExpectedDistance / totalDistance;

            ((MyChromosome)chromosome).Distance = totalDistance;

            if (fitness > 1.0)
                return 1.0;
            return fitness;
        }
    }
}