﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using GeneticSharp.Domain;
using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Crossovers;
using GeneticSharp.Domain.Mutations;
using GeneticSharp.Domain.Populations;
using GeneticSharp.Domain.Selections;
using GeneticSharp.Domain.Terminations;
using GeneticSharp.Extensions;
using GeneticSharp.Extensions.Tsp;
using msiag.Models;
using msiag.Views;

namespace msiag.Presenters
{
    public class AppPresenter
    {
        private readonly IAppFormView _view;

        private List<City> cities;
        private Population population;

        private MyChromosome chromosome;
        private MyFitness fitness;

        private ISelection selection;
        private IMutation mutation;
        private ITermination termination;


        private BackgroundWorker bgWorker;
        private GeneticAlgorithm ga;

        public AppPresenter(IAppFormView view)
        {
            _view = view;
            _view.Presenter = this;

            bgWorker = new BackgroundWorker()
            {
                WorkerReportsProgress = true
            };
            bgWorker.ProgressChanged += BgWorker_ProgressChanged;
            bgWorker.RunWorkerCompleted += BgWorker_RunWorkerCompleted;
            bgWorker.DoWork += BgWorker_DoWork;
        }

        private void BgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            
            ga.Start();
        }

        private void BgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _view.WorkerEnd();
        }

        private void BgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            GeneticAlgorithm ga = (GeneticAlgorithm)e.UserState;
            

            double best = (double)ga.BestChromosome.Fitness;
            double average = (double)ga.Population.CurrentGeneration.Chromosomes.Select(c => c.Fitness).Average();
            double worst = (double)ga.Population.CurrentGeneration.Chromosomes.Select(c => c.Fitness).Min();

            _view.AddBestFirness(ga.GenerationsNumber, best);
            _view.AddAverageFitness(ga.GenerationsNumber, average);
            _view.AddWorstFitness(ga.GenerationsNumber, worst);


            if(e.ProgressPercentage >= 100)
            {
                MyChromosome chromosome = (MyChromosome)ga.BestChromosome;
                var genes = chromosome.GetGenes();

                _view.AddOutputNewLine($"Dystans: { chromosome.Distance}");

                foreach (Gene gene in genes)
                    _view.AddOutputNewLine((gene.Value as City).ToString());
                _view.AddOutputNewLine((genes.First().Value as City).ToString());
            }
        }

        public void Start()
        {
            cities = new List<City>();
            LoadSource();

            fitness = new MyFitness(_view.ExpectedResult, cities);
            chromosome = new MyChromosome(cities.Count());

            CreateAdamChromosome(chromosome);

            ICrossover crossover;

            switch (_view.CrossoverType)
            {
                case 0: crossover = new AlternatingPositionCrossover(); break;
                case 1: crossover = new CycleCrossover(); break;
                case 2: crossover = new OrderedCrossover(); break;
                case 3: crossover = new OrderBasedCrossover(); break;
                case 4: crossover = new PartiallyMappedCrossover(); break;
                case 5: crossover = new PositionBasedCrossover(); break;
                case 7: crossover = new CowgcCrossover(); break;
                default: crossover = new AlternatingPositionCrossover(); break;
            }

            mutation = new ReverseSequenceMutation();
            

            switch(_view.ParentSelectionMode)
            {
                case 0: selection = new RouletteWheelSelection();break;
                case 1: selection = new StochasticUniversalSamplingSelection(); break;
                case 2: selection = new TournamentSelection(); break;
                default: selection = new RouletteWheelSelection(); break;
            }

            var population = new Population(_view.MinPopulationSize, _view.MaxPopulationSize, chromosome);


            ga = new GeneticAlgorithm(population, fitness, selection, crossover, mutation)
            {
                MutationProbability = _view.MutationProbability,
                CrossoverProbability = _view.CrossOverProbability
            };


            switch (_view.TerminationMode)
            {
                case 0: termination = new GenerationNumberTermination(_view.TerminationByGenerationNumber); break;
                case 1: termination = new TimeEvolvingTermination(new TimeSpan(0, 0, _view.TerminationByTimeEvolving)); break;
                case 2: termination = new FitnessStagnationTermination(_view.TerminationByFitnessStagnation); break;
                case 3: termination = new FitnessThresholdTermination(_view.TerminationByFitnessThreshold); break;
                default: termination = new GenerationNumberTermination(100); break;
            }

            ga.Termination = termination;

            ga.GenerationRan += Ga_GenerationRan;
            ga.TerminationReached += Ga_TerminationReached;

            _view.WorkerBegin();

            bgWorker.RunWorkerAsync();
        }

        private void Ga_TerminationReached(object sender, EventArgs e)
        {
            bgWorker.ReportProgress(100, sender);
        }

        private void Ga_GenerationRan(object sender, EventArgs e)
        {
            bgWorker.ReportProgress(0, sender);
        }

       
        private void CreateAdamChromosome(MyChromosome chromosome)
        {
            for (int i = 0; i < cities.Count; i++)
                chromosome.ReplaceGene(i, new Gene(cities[i]));
        }

        private string GetSourcePath()
        {
            switch(_view.Source)
            {
                case 0: return msiag.Properties.Resources.distances_pl; break;
                case 1: return msiag.Properties.Resources.distances_usa; break;
                case 2: return msiag.Properties.Resources.distance_gr17; break;
                default: return msiag.Properties.Resources.distances_pl;
            }
            return msiag.Properties.Resources.distances_pl;
        }

        private void LoadSource()
        {
            CsvConfiguration configuration = new CsvConfiguration(CultureInfo.InvariantCulture) { Delimiter = ";", HasHeaderRecord = false};

            using (var reader = new StringReader(GetSourcePath()))
            using (var csv = new CsvReader(reader, configuration))
            {
                while (csv.Read())
                {
                    City newCity = new City(csv.Context.Row -1, csv.GetField(0));

                    for (int i = 1; i < csv.Context.Record.Length; i++)
                        newCity.Distances.Add(Int32.Parse(csv.GetField(i)));

                    cities.Add(newCity);
                }
            }
        }
    }
}
