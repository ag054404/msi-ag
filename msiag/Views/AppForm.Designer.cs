﻿namespace msiag.Views
{
    partial class AppForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nudExpectedResult = new System.Windows.Forms.NumericUpDown();
            this.rbSourceGr17 = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.rbSourceUSA = new System.Windows.Forms.RadioButton();
            this.rbSourcePL = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.nudMaxPopulationSize = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.nudMinPopulationSize = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.nudStopTresholdValue = new System.Windows.Forms.NumericUpDown();
            this.rbStopTreshold = new System.Windows.Forms.RadioButton();
            this.nudStopStagnationValue = new System.Windows.Forms.NumericUpDown();
            this.rbStopStagnation = new System.Windows.Forms.RadioButton();
            this.nudStopTimeValue = new System.Windows.Forms.NumericUpDown();
            this.rbStopTime = new System.Windows.Forms.RadioButton();
            this.nudStopGenerationValue = new System.Windows.Forms.NumericUpDown();
            this.rbStopGenerationCount = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbCrossoverType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.nudCop = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cbSelections = new System.Windows.Forms.ComboBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.nudSwapMutate = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.rtbOutput = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExpectedResult)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxPopulationSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinPopulationSize)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudStopTresholdValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStopStagnationValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStopTimeValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStopGenerationValue)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCop)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSwapMutate)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tabControl1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(884, 648);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBox2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.groupBox3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.groupBox4, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.groupBox5, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnStart, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.groupBox6, 0, 5);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 7;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 97F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 119F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(244, 642);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nudExpectedResult);
            this.groupBox1.Controls.Add(this.rbSourceGr17);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.rbSourceUSA);
            this.groupBox1.Controls.Add(this.rbSourcePL);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(238, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Źródło";
            // 
            // nudExpectedResult
            // 
            this.nudExpectedResult.Location = new System.Drawing.Point(137, 66);
            this.nudExpectedResult.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.nudExpectedResult.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudExpectedResult.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudExpectedResult.Name = "nudExpectedResult";
            this.nudExpectedResult.Size = new System.Drawing.Size(88, 20);
            this.nudExpectedResult.TabIndex = 5;
            this.nudExpectedResult.ThousandsSeparator = true;
            this.nudExpectedResult.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            // 
            // rbSourceGr17
            // 
            this.rbSourceGr17.AutoSize = true;
            this.rbSourceGr17.Location = new System.Drawing.Point(16, 69);
            this.rbSourceGr17.Name = "rbSourceGr17";
            this.rbSourceGr17.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.rbSourceGr17.Size = new System.Drawing.Size(63, 17);
            this.rbSourceGr17.TabIndex = 2;
            this.rbSourceGr17.Text = "GR17";
            this.rbSourceGr17.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(106, 46);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label8.Size = new System.Drawing.Size(126, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Oczekiwana odległość";
            // 
            // rbSourceUSA
            // 
            this.rbSourceUSA.AutoSize = true;
            this.rbSourceUSA.Location = new System.Drawing.Point(16, 46);
            this.rbSourceUSA.Name = "rbSourceUSA";
            this.rbSourceUSA.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.rbSourceUSA.Size = new System.Drawing.Size(57, 17);
            this.rbSourceUSA.TabIndex = 1;
            this.rbSourceUSA.Text = "USA";
            this.rbSourceUSA.UseVisualStyleBackColor = true;
            // 
            // rbSourcePL
            // 
            this.rbSourcePL.AutoSize = true;
            this.rbSourcePL.Checked = true;
            this.rbSourcePL.Location = new System.Drawing.Point(6, 23);
            this.rbSourcePL.Name = "rbSourcePL";
            this.rbSourcePL.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.rbSourcePL.Size = new System.Drawing.Size(67, 17);
            this.rbSourcePL.TabIndex = 0;
            this.rbSourcePL.TabStop = true;
            this.rbSourcePL.Text = "Polska";
            this.rbSourcePL.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.nudMaxPopulationSize);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.nudMinPopulationSize);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 103);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(238, 91);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Populacja";
            // 
            // nudMaxPopulationSize
            // 
            this.nudMaxPopulationSize.Location = new System.Drawing.Point(168, 45);
            this.nudMaxPopulationSize.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.nudMaxPopulationSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudMaxPopulationSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMaxPopulationSize.Name = "nudMaxPopulationSize";
            this.nudMaxPopulationSize.Size = new System.Drawing.Size(56, 20);
            this.nudMaxPopulationSize.TabIndex = 5;
            this.nudMaxPopulationSize.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 47);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label2.Size = new System.Drawing.Size(156, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Maksymalny rozmiar populacji";
            // 
            // nudMinPopulationSize
            // 
            this.nudMinPopulationSize.Location = new System.Drawing.Point(168, 19);
            this.nudMinPopulationSize.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.nudMinPopulationSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudMinPopulationSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMinPopulationSize.Name = "nudMinPopulationSize";
            this.nudMinPopulationSize.Size = new System.Drawing.Size(56, 20);
            this.nudMinPopulationSize.TabIndex = 3;
            this.nudMinPopulationSize.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label1.Size = new System.Drawing.Size(144, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Minimalny rozmiar populacji";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.nudStopTresholdValue);
            this.groupBox3.Controls.Add(this.rbStopTreshold);
            this.groupBox3.Controls.Add(this.nudStopStagnationValue);
            this.groupBox3.Controls.Add(this.rbStopStagnation);
            this.groupBox3.Controls.Add(this.nudStopTimeValue);
            this.groupBox3.Controls.Add(this.rbStopTime);
            this.groupBox3.Controls.Add(this.nudStopGenerationValue);
            this.groupBox3.Controls.Add(this.rbStopGenerationCount);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 200);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(238, 113);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Warunek stopu";
            // 
            // nudStopTresholdValue
            // 
            this.nudStopTresholdValue.Location = new System.Drawing.Point(137, 85);
            this.nudStopTresholdValue.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.nudStopTresholdValue.Name = "nudStopTresholdValue";
            this.nudStopTresholdValue.Size = new System.Drawing.Size(88, 20);
            this.nudStopTresholdValue.TabIndex = 10;
            this.nudStopTresholdValue.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // rbStopTreshold
            // 
            this.rbStopTreshold.AutoSize = true;
            this.rbStopTreshold.Location = new System.Drawing.Point(14, 85);
            this.rbStopTreshold.Name = "rbStopTreshold";
            this.rbStopTreshold.Size = new System.Drawing.Size(96, 17);
            this.rbStopTreshold.TabIndex = 9;
            this.rbStopTreshold.Text = "Limit fitness [%]";
            this.rbStopTreshold.UseVisualStyleBackColor = true;
            // 
            // nudStopStagnationValue
            // 
            this.nudStopStagnationValue.Location = new System.Drawing.Point(137, 62);
            this.nudStopStagnationValue.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.nudStopStagnationValue.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudStopStagnationValue.Name = "nudStopStagnationValue";
            this.nudStopStagnationValue.Size = new System.Drawing.Size(88, 20);
            this.nudStopStagnationValue.TabIndex = 8;
            this.nudStopStagnationValue.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // rbStopStagnation
            // 
            this.rbStopStagnation.AutoSize = true;
            this.rbStopStagnation.Location = new System.Drawing.Point(14, 62);
            this.rbStopStagnation.Name = "rbStopStagnation";
            this.rbStopStagnation.Size = new System.Drawing.Size(73, 17);
            this.rbStopStagnation.TabIndex = 7;
            this.rbStopStagnation.Text = "Stagnacja";
            this.rbStopStagnation.UseVisualStyleBackColor = true;
            // 
            // nudStopTimeValue
            // 
            this.nudStopTimeValue.Location = new System.Drawing.Point(137, 39);
            this.nudStopTimeValue.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.nudStopTimeValue.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudStopTimeValue.Name = "nudStopTimeValue";
            this.nudStopTimeValue.Size = new System.Drawing.Size(88, 20);
            this.nudStopTimeValue.TabIndex = 6;
            this.nudStopTimeValue.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // rbStopTime
            // 
            this.rbStopTime.AutoSize = true;
            this.rbStopTime.Location = new System.Drawing.Point(14, 39);
            this.rbStopTime.Name = "rbStopTime";
            this.rbStopTime.Size = new System.Drawing.Size(97, 17);
            this.rbStopTime.TabIndex = 5;
            this.rbStopTime.Text = "Czas [sekundy]";
            this.rbStopTime.UseVisualStyleBackColor = true;
            // 
            // nudStopGenerationValue
            // 
            this.nudStopGenerationValue.Location = new System.Drawing.Point(137, 16);
            this.nudStopGenerationValue.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.nudStopGenerationValue.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudStopGenerationValue.Name = "nudStopGenerationValue";
            this.nudStopGenerationValue.Size = new System.Drawing.Size(88, 20);
            this.nudStopGenerationValue.TabIndex = 4;
            this.nudStopGenerationValue.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // rbStopGenerationCount
            // 
            this.rbStopGenerationCount.AutoSize = true;
            this.rbStopGenerationCount.Checked = true;
            this.rbStopGenerationCount.Location = new System.Drawing.Point(14, 16);
            this.rbStopGenerationCount.Name = "rbStopGenerationCount";
            this.rbStopGenerationCount.Size = new System.Drawing.Size(93, 17);
            this.rbStopGenerationCount.TabIndex = 0;
            this.rbStopGenerationCount.TabStop = true;
            this.rbStopGenerationCount.Text = "Ilość generacji";
            this.rbStopGenerationCount.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbCrossoverType);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.nudCop);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 319);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(238, 149);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Operator Crossingover";
            // 
            // cbCrossoverType
            // 
            this.cbCrossoverType.FormattingEnabled = true;
            this.cbCrossoverType.Items.AddRange(new object[] {
            "Alternating-position (AP)",
            "Cycle (CX)",
            "Ordered (OX1)",
            "Order-based (OX2)",
            "Partially Mapped (PMX)",
            "Position-based (POS)",
            "---",
            "COWGC"});
            this.cbCrossoverType.Location = new System.Drawing.Point(10, 64);
            this.cbCrossoverType.Name = "cbCrossoverType";
            this.cbCrossoverType.Size = new System.Drawing.Size(218, 21);
            this.cbCrossoverType.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(197, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "[ % ]";
            // 
            // nudCop
            // 
            this.nudCop.Location = new System.Drawing.Point(131, 19);
            this.nudCop.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.nudCop.Name = "nudCop";
            this.nudCop.Size = new System.Drawing.Size(63, 20);
            this.nudCop.TabIndex = 11;
            this.nudCop.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 21);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label7.Size = new System.Drawing.Size(119, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Prawdopodonieństwo";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cbSelections);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(3, 474);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(238, 62);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Wybór rodzica";
            // 
            // cbSelections
            // 
            this.cbSelections.FormattingEnabled = true;
            this.cbSelections.Items.AddRange(new object[] {
            "Roulette Wheel",
            "Stochastic Universal Sampling",
            "Tournament"});
            this.cbSelections.Location = new System.Drawing.Point(6, 19);
            this.cbSelections.Name = "cbSelections";
            this.cbSelections.Size = new System.Drawing.Size(218, 21);
            this.cbSelections.TabIndex = 0;
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStart.Location = new System.Drawing.Point(30, 610);
            this.btnStart.Margin = new System.Windows.Forms.Padding(30, 3, 30, 3);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(184, 23);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.nudSwapMutate);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(3, 542);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(238, 57);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Pozostałe operatory";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(203, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "[ % ]";
            // 
            // nudSwapMutate
            // 
            this.nudSwapMutate.Location = new System.Drawing.Point(102, 14);
            this.nudSwapMutate.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.nudSwapMutate.Name = "nudSwapMutate";
            this.nudSwapMutate.Size = new System.Drawing.Size(88, 20);
            this.nudSwapMutate.TabIndex = 7;
            this.nudSwapMutate.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 16);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Mutacja";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(253, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(628, 642);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.chart1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(620, 616);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Wykres fitness";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // chart1
            // 
            chartArea1.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(3, 3);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "Najlepszy";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "Średni";
            series2.YValuesPerPoint = 2;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "Najgorszy";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(614, 610);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.rtbOutput);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(620, 616);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Trasa";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // rtbOutput
            // 
            this.rtbOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbOutput.Location = new System.Drawing.Point(3, 3);
            this.rtbOutput.Name = "rtbOutput";
            this.rtbOutput.ReadOnly = true;
            this.rtbOutput.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.rtbOutput.Size = new System.Drawing.Size(614, 610);
            this.rtbOutput.TabIndex = 0;
            this.rtbOutput.Text = "";
            // 
            // AppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(884, 648);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(816, 621);
            this.Name = "AppForm";
            this.Text = "Form1";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExpectedResult)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxPopulationSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinPopulationSize)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudStopTresholdValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStopStagnationValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStopTimeValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStopGenerationValue)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCop)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSwapMutate)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbSourceUSA;
        private System.Windows.Forms.RadioButton rbSourcePL;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.RichTextBox rtbOutput;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown nudMinPopulationSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown nudStopGenerationValue;
        private System.Windows.Forms.RadioButton rbStopGenerationCount;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown nudExpectedResult;
        private System.Windows.Forms.RadioButton rbSourceGr17;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudCop;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudSwapMutate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbSelections;
        private System.Windows.Forms.NumericUpDown nudMaxPopulationSize;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudStopTresholdValue;
        private System.Windows.Forms.RadioButton rbStopTreshold;
        private System.Windows.Forms.NumericUpDown nudStopStagnationValue;
        private System.Windows.Forms.RadioButton rbStopStagnation;
        private System.Windows.Forms.NumericUpDown nudStopTimeValue;
        private System.Windows.Forms.RadioButton rbStopTime;
        private System.Windows.Forms.ComboBox cbCrossoverType;
    }
}

