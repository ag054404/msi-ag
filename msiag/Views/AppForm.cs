﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace msiag.Views
{
    public partial class AppForm : Form, IAppFormView
    {
        public Presenters.AppPresenter Presenter { private get; set; }

        public int Source { private set { } get
            {
                if (rbSourcePL.Checked)
                    return 0;
                else if (rbSourceUSA.Checked)
                    return 1;
                else if (rbSourceGr17.Checked)
                    return 2;
                else
                    return 0;
            }
        }

        public int MinPopulationSize { 
            get
            {
                return (int)nudMinPopulationSize.Value;
            }
        }

        public int MaxPopulationSize
        {
            get
            {
                return (int)nudMaxPopulationSize.Value;
            }
        }

        public int StopByGenerationCount
            {
                get => (int) nudStopGenerationValue.Value;
            }

         public int CrossoverType
         {
            get
            {
                return cbCrossoverType.SelectedIndex;
            }
        }

        public int ParentSelectionMode
        {
            get
            {
                return cbSelections.SelectedIndex;
            }
        }

        public int ExpectedResult
        {
            get => (int)nudExpectedResult.Value;
        }

        public float MutationProbability { get => (float)nudSwapMutate.Value / 100; }
        public float CrossOverProbability { get => (float)nudCop.Value / 100; }

        public int TerminationMode
        {
            get
            {
                if (rbStopGenerationCount.Checked)
                    return 0;
                else if (rbStopTime.Checked)
                    return 1;
                else if (rbStopStagnation.Checked)
                    return 2;
                else if (rbStopTreshold.Checked)
                    return 3;
                else
                    return 0;
            }
        }
        public int TerminationByGenerationNumber { get => (int)nudStopGenerationValue.Value; }

        public int TerminationByTimeEvolving { get => (int)nudStopTimeValue.Value; }

        public int TerminationByFitnessStagnation { get => (int)nudStopStagnationValue.Value; }

        public float TerminationByFitnessThreshold { get => (float)nudStopTresholdValue.Value / 100; }

        public AppForm()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            rtbOutput.Text = "";

            foreach (var series in chart1.Series)
                series.Points.Clear();

           
            

            Presenter.Start();
            //try
            //{
            //    Presenter.Start();
            //}
            //catch(Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Błąd" , MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            
        }

        public void AddOutputLine(string line)
        {
            rtbOutput.Text += $"{line}";
        }

        public void AddOutputNewLine(string line)
        {
            rtbOutput.Text += $"{line}\n";
        }

        public void AddBestFirness(int generation, double fitness)
        {
            chart1.Series[0].Points.Add(new System.Windows.Forms.DataVisualization.Charting.DataPoint(generation, fitness));
        }
        public void AddWorstFitness(int generation, double fitness)
        {
            chart1.Series[2].Points.Add(new System.Windows.Forms.DataVisualization.Charting.DataPoint(generation, fitness));
        }
        public void AddAverageFitness(int generation, double fitness)
        {
            chart1.Series[1].Points.Add(new System.Windows.Forms.DataVisualization.Charting.DataPoint(generation, fitness));
        }

        public void WorkerBegin()
        {
            this.Enabled = false;
        }

        public void WorkerEnd()
        {
            this.Enabled = true;
        }
    }
}
