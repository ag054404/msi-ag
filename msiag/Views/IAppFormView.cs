﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace msiag.Views
{
    public interface IAppFormView 
    {
        Presenters.AppPresenter Presenter { set; }
        int Source { get; }
        int MinPopulationSize { get; }
        int MaxPopulationSize { get; }
        int CrossoverType { get; }
        int ParentSelectionMode { get; }

        int TerminationMode { get; }
        int TerminationByGenerationNumber { get; }
        int TerminationByTimeEvolving { get; }
        int TerminationByFitnessStagnation { get; }
        float TerminationByFitnessThreshold { get; }

        int ExpectedResult { get; }

        float MutationProbability { get; }
        float CrossOverProbability { get; }

        void AddOutputLine(string line);
        void AddOutputNewLine(string line);

        void AddBestFirness(int generation, double fitness);
        void AddWorstFitness(int generation, double distance);
        void AddAverageFitness(int generation, double distance);

        void WorkerBegin();
        void WorkerEnd();
    }
}
